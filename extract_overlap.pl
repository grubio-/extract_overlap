#! /usr/bin/perl 

# Copyright 2014 Ansgar Gruber
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#   http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

use warnings;
use strict;

# extract_overlap.pl counts all possible overlapping groups between lists of elements read from input files and prints the summary into an output file, the counts may be used to create Venn diagrams. Multiple occurences of elements within one file are counted only once. The identified subsets are printed into individual output files.
#
# a_total
# b_total
# c_total
# a_exclusive
# b_exclusive
# c_exclusive
# ab_overlap
# ac_overlap
# bc_overlap
# abc_overlap

# open and read files with elements to be compared
my $a_total_fn = "a_total_fn_initial";
my $b_total_fn = "b_total_fn_initial";
my $c_total_fn = "c_total_fn_initial";
if ( scalar(@ARGV) == 3 ) {
    $a_total_fn = shift(@ARGV);
    chomp($a_total_fn);
    unwhite($a_total_fn);
    $b_total_fn = shift(@ARGV);
    chomp($b_total_fn);
    unwhite($b_total_fn);
    $c_total_fn = shift(@ARGV);
    chomp($c_total_fn);
    unwhite($c_total_fn);
}
else {
    die(
"\nFailed to identify the names of the files that contain the elements to be compared for this run of extract_overlap.pl.\n, Usage 'perl extract_overlap.pl a_total b_total c_total (text file, one element per line, specify full file name with extension), if run from Windows cmd, append '>log.txt 2>&1' to redirect output into 'log.txt'.\n"
    );
}

my %total = ();    # all elements as keys, to cross-check if defined

open( my $a_total_fh, "<", $a_total_fn )
  or die( "program died: unable to open "
      . $a_total_fn
      . ", please check filename.\n" );
my %a_total = ();    # elements as keys, to cross-check if defined
while ( my $element = <$a_total_fh> ) {
    chomp($element);
    unwhite($element);    # remove any remaining whitespace if present
    $a_total{ lc($element) } = 0;
    $total{ lc($element) }   = 0;

# lower case is necessary because case insensitive matching does not work on bracketed character classes in Perl
}
close($a_total_fh);

open( my $b_total_fh, "<", $b_total_fn )
  or die( "program died: unable to open "
      . $b_total_fn
      . ", please check filename.\n" );
my %b_total = ();         # elements as keys, to cross-check if defined
while ( my $element = <$b_total_fh> ) {
    chomp($element);
    unwhite($element);    # remove any remaining whitespace if present
    $b_total{ lc($element) } = 0;
    $total{ lc($element) }   = 0;

# lower case is necessary because case insensitive matching does not work on bracketed character classes in Perl
}
close($b_total_fh);

open( my $c_total_fh, "<", $c_total_fn )
  or die( "program died: unable to open "
      . $c_total_fn
      . ", please check filename.\n" );
my %c_total = ();         # elements as keys, to cross-check if defined
while ( my $element = <$c_total_fh> ) {
    chomp($element);
    unwhite($element);    # remove any remaining whitespace if present
    $c_total{ lc($element) } = 0;
    $total{ lc($element) }   = 0;

# lower case is necessary because case insensitive matching does not work on bracketed character classes in Perl
}
close($c_total_fh);

my %a_exclusive    = ();
my %b_exclusive    = ();
my %c_exclusive    = ();
my %ab_overlap  = ();
my %ac_overlap  = ();
my %bc_overlap  = ();
my %abc_overlap = ();

foreach my $element ( keys(%total) ) {
    if (    ( defined( $a_total{$element} ) )
        and ( defined( $b_total{$element} ) )
        and ( defined( $c_total{$element} ) ) )
    {
        $abc_overlap{$element} = "0";
        $total{$element}       = "abc";
        $a_total{$element}     = "abc";
        $b_total{$element}     = "abc";
        $c_total{$element}     = "abc";
    }
    elsif ( ( defined( $a_total{$element} ) )
        and ( defined( $b_total{$element} ) ) )
    {
        $ab_overlap{$element} = "0";
        $total{$element}      = "ab";
        $a_total{$element}    = "ab";
        $b_total{$element}    = "ab";
    }
    elsif ( ( defined( $a_total{$element} ) )
        and ( defined( $c_total{$element} ) ) )
    {
        $ac_overlap{$element} = "0";
        $total{$element}      = "ac";
        $a_total{$element}    = "ac";
        $c_total{$element}    = "ac";
    }
    elsif ( ( defined( $b_total{$element} ) )
        and ( defined( $c_total{$element} ) ) )
    {
        $bc_overlap{$element} = "0";
        $total{$element}      = "bc";
        $b_total{$element}    = "bc";
        $c_total{$element}    = "bc";
    }
    elsif ( defined( $a_total{$element} ) ) {
        $a_exclusive{$element} = "0";
        $total{$element}    = "a";
        $a_total{$element}  = "a";
    }
    elsif ( defined( $b_total{$element} ) ) {
        $b_exclusive{$element} = "0";
        $total{$element}    = "b";
        $b_total{$element}  = "b";
    }
    elsif ( defined( $c_total{$element} ) ) {
        $c_exclusive{$element} = "0";
        $total{$element}    = "c";
        $c_total{$element}  = "c";
    }
    else {
        die("strange exception: unexpected element identified.\n");
    }
}

my $summary_fn = "extract_overlap_summary.txt";
open( my $summary_write_fh, ">", $summary_fn )
  or die( "program died: unable to open " . $summary_fn . ".\n" );
print $summary_write_fh (
        scalar( keys(%total) )
      . " elements read from "
      . $a_total_fn . ", "
      . $b_total_fn . " and "
      . $c_total_fn
      . ".\n\nThese fall into the following sub-sets:\n\n"
      . $a_total_fn
      . " total: "
      . scalar( keys(%a_total) ) . "\n"
      . $b_total_fn
      . " total: "
      . scalar( keys(%b_total) ) . "\n"
      . $c_total_fn
      . " total: "
      . scalar( keys(%c_total) ) . "\n"
      . $a_total_fn
      . " exclusive: "
      . scalar( keys(%a_exclusive) ) . "\n"
      . $b_total_fn
      . " exclusive: "
      . scalar( keys(%b_exclusive) ) . "\n"
      . $c_total_fn
      . " exclusive: "
      . scalar( keys(%c_exclusive) ) . "\n"
      . $a_total_fn . " and "
      . $b_total_fn
      . " overlap: "
      . scalar( keys(%ab_overlap) ) . "\n"
      . $a_total_fn . " and "
      . $c_total_fn
      . " overlap: "
      . scalar( keys(%ac_overlap) ) . "\n"
      . $b_total_fn . " and "
      . $c_total_fn
      . " overlap: "
      . scalar( keys(%bc_overlap) ) . "\n"
      . $a_total_fn . " and "
      . $b_total_fn . " and "
      . $c_total_fn
      . " overlap: "
      . scalar( keys(%abc_overlap) ) . "\n"
      . "\nHere are the control sums:\n\n"
      . (
        scalar( keys(%a_exclusive) ) +
          scalar( keys(%b_exclusive) ) +
          scalar( keys(%c_exclusive) ) +
          scalar( keys(%ab_overlap) ) +
          scalar( keys(%ac_overlap) ) +
          scalar( keys(%bc_overlap) ) +
          scalar( keys(%abc_overlap) )
      )
      . " = "
      . scalar( keys(%total) ) . "\n"
      . (
        scalar( keys(%a_exclusive) ) +
          scalar( keys(%ab_overlap) ) +
          scalar( keys(%ac_overlap) ) +
          scalar( keys(%abc_overlap) )
      )
      . " = "
      . scalar( keys(%a_total) ) . "\n"
      . (
        scalar( keys(%b_exclusive) ) +
          scalar( keys(%ab_overlap) ) +
          scalar( keys(%bc_overlap) ) +
          scalar( keys(%abc_overlap) )
      )
      . " = "
      . scalar( keys(%b_total) ) . "\n"
      . (
        scalar( keys(%c_exclusive) ) +
          scalar( keys(%ac_overlap) ) +
          scalar( keys(%bc_overlap) ) +
          scalar( keys(%abc_overlap) )
      )
      . " = "
      . scalar( keys(%c_total) ) . "\n"
);
close($summary_write_fh);

# print the individual sub-set compositions into individual files
my $a_fn =
  substr( $a_total_fn, 0, ( length($a_total_fn) - 4 ) ) . "_exclusive.txt";
my $b_fn =
  substr( $b_total_fn, 0, ( length($b_total_fn) - 4 ) ) . "_exclusive.txt";
my $c_fn =
  substr( $c_total_fn, 0, ( length($c_total_fn) - 4 ) ) . "_exclusive.txt";
my $ab_fn =
    substr( $a_total_fn, 0, ( length($a_total_fn) - 4 ) ) . "_"
  . substr( $b_total_fn, 0, ( length($b_total_fn) - 4 ) )
  . "_overlap.txt";
my $ac_fn =
    substr( $a_total_fn, 0, ( length($a_total_fn) - 4 ) ) . "_"
  . substr( $c_total_fn, 0, ( length($c_total_fn) - 4 ) )
  . "_overlap.txt";
my $bc_fn =
    substr( $b_total_fn, 0, ( length($b_total_fn) - 4 ) ) . "_"
  . substr( $c_total_fn, 0, ( length($c_total_fn) - 4 ) )
  . "_overlap.txt";
my $abc_fn =
    substr( $a_total_fn, 0, ( length($a_total_fn) - 4 ) ) . "_"
  . substr( $b_total_fn, 0, ( length($b_total_fn) - 4 ) ) . "_"
  . substr( $c_total_fn, 0, ( length($c_total_fn) - 4 ) )
  . "_overlap.txt";

open( my $a_fh, ">", $a_fn )
  or die( "program died: unable to open " . $a_fn . ".\n" );
print $a_fh( join( "\n", keys(%a_exclusive) ) );
close($a_fh);
open( my $b_fh, ">", $b_fn )
  or die( "program died: unable to open " . $b_fn . ".\n" );
print $b_fh( join( "\n", keys(%b_exclusive) ) );
close($b_fh);
open( my $c_fh, ">", $c_fn )
  or die( "program died: unable to open " . $c_fn . ".\n" );
print $c_fh( join( "\n", keys(%c_exclusive) ) );
close($c_fh);
open( my $ab_fh, ">", $ab_fn )
  or die( "program died: unable to open " . $ab_fn . ".\n" );
print $ab_fh( join( "\n", keys(%ab_overlap) ) );
close($ab_fh);
open( my $ac_fh, ">", $ac_fn )
  or die( "program died: unable to open " . $ac_fn . ".\n" );
print $ac_fh( join( "\n", keys(%ac_overlap) ) );
close($ac_fh);
open( my $bc_fh, ">", $bc_fn )
  or die( "program died: unable to open " . $bc_fn . ".\n" );
print $bc_fh( join( "\n", keys(%bc_overlap) ) );
close($bc_fh);
open( my $abc_fh, ">", $abc_fn )
  or die( "program died: unable to open " . $abc_fn . ".\n" );
print $abc_fh( join( "\n", keys(%abc_overlap) ) );
close($abc_fh);

print(
"\nThank you for using extract_overlap.pl, please contact ansgar.gruber\@uni-konstanz.de \nwith any question concerning the program :-\)"
);

sub unwhite
{ # removes any whitespace (including linebreaks) from the beginning and the end of the scalar value passed to it
    unless ( scalar(@_) == 1 ) {
        die(
"Subroutine unwhite needs exactly one element of \@\_ (from wich any whitespace that might occur at the end will be removed), program died, please check.\n"
        );
    }
    else {
        $_[0] =~ s/^\s*//g;
        $_[0] =~ s/\s*$//g;
        return ( $_[0] );
    }
}    # end of sub unwhite
